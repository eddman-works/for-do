async function postData() {
  await fetch('https://eddfit.ru/test/menu.php')
    .then(respons => respons.json())
    .then(data => {
        console.log(menu(data.result));
    });
};

const menu = (object) => {
    let product = [];
    object.products.forEach(element => {
        character = { id: element.id, name: element.name, category: list(object.categories, element.categoryId), price: element.price };
        product.push(character);
    });
    return product;
};

let list = (categories, categoryId) => {
    let category = {};
    categories.forEach(element => {
        if (element.id == categoryId) {
            category["id"] = categoryId;
            category["subCategory"] = element.name;
            if (element.parentId != null)
            category["category"] = list(categories, element.parentId);
        }
    });
    return category;
}

postData();